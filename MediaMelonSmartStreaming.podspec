
Pod::Spec.new do |spec|

spec.platform = :ios
spec.ios.deployment_target = "9.0"

spec.name = "MediaMelonSmartStreaming"
spec.version      = "0.0.3"
spec.summary      = "A short description of MediaMelonSmartStreaming."

spec.description  = "The MediaMelon Player SDK adds SmartSight Analytics and QBR SmartStreaming capability to any media player and is available for all ABR media players."

spec.requires_arc = true

spec.source = { :git => "https://bitbucket.org/neerajk14/mediamelonsmartstreaming.git", :tag => "0.0.3" }

spec.license = { :type => "MIT", :file => "LICENSE" }

spec.author = { "Neeraj" => "neeraj.kumar@tychotechnologies.in" }

spec.homepage = "https://bitbucket.org/neerajk14/mediamelonsmartstreaming.git"

spec.frameworks = "UIKit", "CoreTelephony", "Foundation"


spec.source_files = "**/*.{h,m,swift}"

spec.public_header_files = "**/*.h"

spec.swift_version = "4.0"

spec.ios.vendored_libraries = "**/*.a"

spec.libraries = "stdc++"


spec.pod_target_xcconfig = { "VALID_ARCHS[sdk=iphonesimulator*]" => "armv7 arm64 x86_64"}

end
