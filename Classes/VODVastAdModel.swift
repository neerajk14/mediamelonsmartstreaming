//
//  VODVastAdModel.swift
//  VOPlayerDemoSwift
//
//  Created by Anurag Singh on 4/2/21.
//  Copyright © 2021 VOPlayer. All rights reserved.
//

import Foundation

struct VODVastAdModel: Codable {
    
    var category: String?
    var titleId: String?
    var title: String?
    var duration: u_long?
    var impressionUrlTemplates: [String]?
    var trackingEvents: TrackingURLs?
    
    enum Keys: String, CodingKey {
        case category
        case titleId
        case title
        case duration
        case impressionUrlTemplates
        case trackingEvents
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        
        if container.contains(.category) {
            if let value = try? container.decode(String.self, forKey: .category) { self.category = value }
        }
        if container.contains(.titleId) {
            if let value = try? container.decode(String.self, forKey: .titleId) { self.titleId = value }
        }
        if container.contains(.title) {
            if let value = try? container.decode(String.self, forKey: .title) { self.title = value }
        }
        if container.contains(.duration) {
            if let value = try? container.decode(u_long.self, forKey: .duration) { self.duration = value }
        }
        if container.contains(.impressionUrlTemplates) {
            if let value = try? container.decode([String].self, forKey: .impressionUrlTemplates) { self.impressionUrlTemplates = value }
        }
        if container.contains(.trackingEvents) {
            if let value = try? container.decode(TrackingURLs.self, forKey: .trackingEvents) { self.trackingEvents = value }
        }
    }
}

struct TrackingURLs: Codable {
    
    var start: [String]?
    var firsQuartile: [String]?
    var midpoint: [String]?
    var thirdQuartile: [String]?
    var complete: [String]?
    
    enum Keys: String, CodingKey {
        case start
        case firstQuartile
        case midpoint
        case thirdQuartile
        case complete
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: Keys.self)
        
        if container.contains(.start) {
            if let value = try? container.decode([String].self, forKey: .start) { start = value }
        }
        if container.contains(.firstQuartile) {
            if let value = try? container.decode([String].self, forKey: .firstQuartile) { firsQuartile = value }
        }
        if container.contains(.midpoint) {
            if let value = try? container.decode([String].self, forKey: .midpoint) { midpoint = value }
        }
        if container.contains(.thirdQuartile) {
            if let value = try? container.decode([String].self, forKey: .thirdQuartile) { thirdQuartile = value }
        }
        if container.contains(.complete) {
            if let value = try? container.decode([String].self, forKey: .complete) { complete = value }
        }
    }
}
