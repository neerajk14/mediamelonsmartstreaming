//
//  MMSSAIAdManager.swift
//  VOPlayerDemoSwift
//
//  Created by Anurag Singh on 3/26/21.
//  Copyright © 2021 VOPlayer. All rights reserved.
//

import Foundation

protocol MMSSAIDelegate {
    func notifyAdEventWith(state: MMAdState, vastAd: VastAd?, vastTracker: VastTracker, andAdIndex index: Int)
    func notifySSAIAdEventWith(state: MMAdState, adInfo: MMSSAIAdInfo)
}

class MMSSAIAdManager: NSObject {
    //MARK:- OBJECTS
    @objc public var isAdManagerSet = false
    public var delegate: MMSSAIDelegate?
    private var stringMediaURL: String!
    private var stringVastURL: String!
    private var manifestTargetDuration = 0
    private var currentAdStartTime = 0
    private var currentAdEndTime = 0
    private var vastClient = VastClient()
    private var vastTracker: VastTracker?
    private var fakePlayheadProgressTimer: Timer?
    private var playhead = 0.0
    //private var player: VOPlayer!
    private var isLogTraceEnabled = true
    private var timerGetVastResponse: Timer?
    private var timeDelayToCheckVastResponse: Double = 0.0
    private var isAdStarted = false
    private var adIndex = 0
    private var isAdRequested = false
    private var isLive: Bool = false
    private var vodVastModel: [VODVastAdModel]?
    private var adTimeLine: [MMSSAIAdInfo] = []
    private var currentPlayerPosInMs: u_long = 0
    private var livePlayPositionDiffAtSyncMs: u_long = 0
    private var playPositionAtSyncMs: u_long = 0
    private var programDateTimeAtSyncMs: u_long = 0
    private var positionInMs: u_long = 0
    
    //MARK:- INIT
    override init() {
        super.init()
    }
        
    //@objc public init(_player: VOPlayer) {
        //super.init()
        //self.player = _player
        //self.playhead = self.player.currentTime
    //}
        
    //MARK:- SET REDIRECTED URL
    @objc public func setupMediaURL(mediaURL: String) {
        self.invalidateTimer()
        self.stringMediaURL = mediaURL
        self.isAdManagerSet = true
        self.parseManifestURL(mediaURL: self.stringMediaURL)
    }
    
    @objc public func setupSSAIAdManager(mediaURL: String, vastUrl: String, isLive: Bool) {
        self.invalidateTimer()
        self.stringMediaURL = mediaURL
        self.stringVastURL = vastUrl
        self.isAdManagerSet = true
        self.isLive = isLive
        if isLive {
            self.parseManifestURL(mediaURL: self.stringMediaURL)
        } else {
            parseVastResponseForVOD(vastUrl: vastUrl)
        }
    }
        
    @objc public func notifyPlayerPlaybackTime(playbackTime: Int) {
        print(playbackTime)
        self.playhead = Double(self.getSyncPosInMs(currentPosInMs: u_long(playbackTime)))
        print(self.playhead)
        let currentPosInMs = u_long(self.playhead)
        for (adIndex, ad) in adTimeLine.enumerated() {
            if ad.adState == "READY" {
                print("******** SSAI AD Loaded ********* ")
            }
            if(currentPosInMs >= ad.startTime && currentPosInMs <= ad.endTime){
                adTimeLine[adIndex].adState = "START"
                print("******** SSAI AD PLAYING ********* \(ad.adId) ")
                adTimeLine[adIndex].active = true;
                self.isAdStarted = true
                if(adTimeLine[adIndex].adTrackerInfo?.isAdImpressionSent == false){
                    print("******** SSAI AD Impression ********* \(ad.adId) ")
                    // Send Impression Becons if present
                    // Send Impression delegate
                    adTimeLine[adIndex].adTrackerInfo?.isAdImpressionSent = true
                    let impressions = adTimeLine[adIndex].adImpressionsTemplates.compactMap { $0.url }
                    track(urls: impressions, eventName: "IMPRESSIONS")
                    self.adImpression(adInfo: adTimeLine[adIndex])
                }
                if(adTimeLine[adIndex].adTrackerInfo?.isAdStartSent == false){
                    print("******** SSAI AD Started ********* \(ad.adId) ")
                    // Send Start Becons if present
                    // Send Start Delegate
                    
                    adTimeLine[adIndex].adTrackerInfo?.isAdStartSent = true
                    callTrackingEventUrlsFor(type: .start, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
                    self.adStart(adInfo: adTimeLine[adIndex])
                }
                if(adTimeLine[adIndex].adTrackerInfo?.isFirstQuartileSent == false && currentPosInMs >= ad.firstQuartile){
                    print("******** SSAI AD FristQ ********* \(ad.adId) ")
                    // Send FirstQuartile Becons if present
                    // Send FirstQuartile Delegate
                    adTimeLine[adIndex].adTrackerInfo?.isFirstQuartileSent = true
                    callTrackingEventUrlsFor(type: .firstQuartile, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
                    self.adFirstQuartile(adInfo: adTimeLine[adIndex])
                }
                if(adTimeLine[adIndex].adTrackerInfo?.isMidPointSent == false && currentPosInMs >= ad.midPoint){
                    print("******** SSAI AD MidPoint ********* \(ad.adId) ")
                    // Send MidPoint Becons if present
                    // Send MidPoint Delegate
                    adTimeLine[adIndex].adTrackerInfo?.isMidPointSent = true
                    callTrackingEventUrlsFor(type: .midpoint, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
                    self.adMidPoint(adInfo: adTimeLine[adIndex])
                }
                if(adTimeLine[adIndex].adTrackerInfo?.isThirdQuartileSent == false && currentPosInMs >= ad.thirdQuartile){
                    print("******** SSAI AD ThridQ ********* \(ad.adId) ")
                    // Send ThirdQuartile Becons if present
                    // Send ThirdQuartile Delegate
                    adTimeLine[adIndex].adTrackerInfo?.isThirdQuartileSent = true
                    callTrackingEventUrlsFor(type: .thirdQuartile, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
                    self.adThirdQuartile(adInfo: adTimeLine[adIndex])
                }
                if(adTimeLine[adIndex].adTrackerInfo?.isAdCompleteSent == false && currentPosInMs >= ad.endTime){
                    adTimeLine[adIndex].adState = "COMPLETE"
                    print("******** SSAI AD Complete ********* \(ad.adId) ")
                    // Send AdComplete Becons if present
                    // Send AdComplete Delegate
                    adTimeLine[adIndex].adTrackerInfo?.isAdCompleteSent = true
                    adTimeLine[adIndex].active = false
                    self.isAdStarted = false
                    callTrackingEventUrlsFor(type: .complete, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
                    self.adComplete(adInfo: adTimeLine[adIndex])
                }
            }
            
            if(adTimeLine[adIndex].adTrackerInfo?.isAdCompleteSent == false && currentPosInMs >= ad.endTime){
                adTimeLine[adIndex].adState = "COMPLETE"
                print("******** SSAI AD Completed 2 ********* \(ad.adId) ")
                // Send AdComplete Becons if present
                // Send AdComplete Delegate
                adTimeLine[adIndex].adTrackerInfo?.isAdCompleteSent = true
                adTimeLine[adIndex].active = false
                self.isAdStarted = false
                self.adComplete(adInfo: adTimeLine[adIndex])
                callTrackingEventUrlsFor(type: .complete, trackingEvents: adTimeLine[adIndex].adTrackingEvents)
            }
            
        }
                /*
                if self.currentAdEndTime <= Int(self.playhead) && self.isAdStarted {
                    //try tracker.trackAdComplete()
                    self.adComplete(vastTracker: tracker, ad: tracker.vastModel.ads[self.adIndex])
                    self.isAdStarted = false
                    self.adIndex = self.adIndex + 1
                    if (self.adIndex == tracker.vastModel.ads.count - 1) {
                        self.adBreakComplete(vastTracker: tracker)
                    }
                } else if Int(self.playhead / 1000) > (self.currentAdStartTime / 1000) && !self.isAdStarted {
                    if (self.adIndex < tracker.vastModel.ads.count) {
                        self.isAdStarted = true
                        if (self.adIndex == 0) {
                            self.adBreakStart(vastTracker: tracker)
                        }
                        try tracker.trackAdStart(withId: tracker.vastModel.ads[self.adIndex].id)
                        self.adStart(vastTracker: tracker, ad: tracker.vastModel.ads[self.adIndex])
                    }
                } else {
                    try tracker.updateProgress(time: self.playhead / 1000)
                }
            } catch TrackingError.unableToUpdateProgress(msg: let msg) {
                print("Tracking Error > Unable to update progress: \(msg)")
                if (self.adIndex == tracker.vastModel.ads.count - 1) {
                    self.adBreakComplete(vastTracker: tracker)
                }
            } catch {
                print("Tracking Error > unknown")
                if (self.adIndex == tracker.vastModel.ads.count - 1) {
                    self.adBreakComplete(vastTracker: tracker)
                }
            }
 */
        //} else {
        //    guard let vastModel = vodVastModel else { return }
        //}
    }
      
    private func callTrackingEventUrlsFor(type: TrackingEventType, trackingEvents: [VastTrackingEvent]) {
        let trackingUrls = trackingEvents
                        .filter { $0.type == type }
                        .compactMap { $0.url }
        if !trackingUrls.isEmpty {
            track(urls: trackingUrls, eventName: type.rawValue.uppercased())
        }
    }
    
    //MARK:- LOG MESSAGE
    private func mmSSAILogger(message: String) {
        if (self.isLogTraceEnabled && !message.isEmpty) {
            print("MM SSAI LOG: ==> \(message)")
        }
    }
        
    //MARK:- PARSE MANIFEST URL
    private func parseManifestURL(mediaURL: String) {
        do {
            let m3u8PlaylistModel = try M3U8PlaylistModel(url: URL(string: mediaURL)!)
            let stringVarientManifestXML = m3u8PlaylistModel.mainMediaPl?.originalText
            if self.stringVastURL.isEmpty {
               self.stringVastURL = (m3u8PlaylistModel.masterPlaylist.allStreamURLs()?.first as! URL).absoluteString + "/vast"
            }
            if let arrayTags = stringVarientManifestXML?.components(separatedBy: "\n") {
                for tag in arrayTags {
                    if tag.contains("#EXT-X-PROGRAM-DATE-TIME") {
                        if let stringPDT = tag.components(separatedBy: "#EXT-X-PROGRAM-DATE-TIME:").last {
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
                            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                            if let programDate = dateFormatter.date(from: stringPDT) {
                                let seconds = programDate.timeIntervalSince1970
                                let pdt = Int(seconds * 1000)
                                self.syncPDTPosition(pdtInMs: u_long(pdt))
                            }
                        }
                    } else if tag.contains("#EXT-X-TARGETDURATION") {
                        if let targetDuration = tag.components(separatedBy: "#EXT-X-TARGETDURATION:").last {
                            self.manifestTargetDuration = Int(targetDuration)!
                            self.timeDelayToCheckVastResponse = Double(targetDuration)! / 2
                            self.invalidateTimer()
                            self.activateTimer()
                            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(self.manifestTargetDuration / 2)) {
                                self.parseManifestURL(mediaURL: self.stringMediaURL)
                            }
                        }
                    }
                }
            }
        } catch {
            print("Could not able to get manifest url")
        }
    }
    
    private func parseVastResponseForVOD(vastUrl: String) {
        let request = URLRequest(url: URL(string: vastUrl)!)
        let session = URLSession(configuration: URLSessionConfiguration.default)

        let task = session.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if error != nil || data == nil {
                    print("Client error!")
                    return
                }
                if let responseData = data {
                    if responseData.count > 0 {
                        let decoder = JSONDecoder()
                        if let responseModel = try? decoder.decode([VODVastAdModel].self, from: responseData) {
                            self.vodVastModel = responseModel
                            var startTimeInMs: u_long = 0
                            var clipTimelineStartTrackerInMs: u_long = 0
                            self.adTimeLine.removeAll()
                            for (index, vastModel) in responseModel.enumerated() {
                                if let category = vastModel.category {
                                    let durationInMs = (vastModel.duration ?? 0 ) * 1000
                                    if category.lowercased() == "ad" {
                                        startTimeInMs = clipTimelineStartTrackerInMs
                                        let adInfo = self.createMMAdInfo(model: vastModel, startTime: startTimeInMs, index: index)
                                        self.adTimeLine.append(adInfo)
                                    }
                                    clipTimelineStartTrackerInMs += durationInMs
                                }
                            }
                            if self.adTimeLine.count > 0 {
                                // Cue ad timeline updated
                            }
                        }
                    }
                }
            }
        }
        task.resume()
    }
    
    private func createMMAdInfo(model: VODVastAdModel, startTime: u_long, index: Int) -> MMSSAIAdInfo {
        var position = "MID"
        if(startTime <= 0){
            position = "PRE";
        } else if(index >= (self.vodVastModel!.count - 1)){
            position = "POST";
        }
        //var vastImpresionTemplete VastImpression
        var impressions = [VastImpression]()
        var trackingEvents = [VastTrackingEvent]()
        if let impressionTemplates = model.impressionUrlTemplates {
            for impression in impressionTemplates {
                impressions.append(VastImpression(urlString: impression))
            }
        }
        if let tEvents = model.trackingEvents {
            for tEvent in tEvents.start ?? [] {
                trackingEvents.append(VastTrackingEvent(urlStr: tEvent, type: .start, isTracked: false))
            }
            for tEvent in tEvents.firsQuartile ?? [] {
                trackingEvents.append(VastTrackingEvent(urlStr: tEvent, type: .firstQuartile, isTracked: false))
            }
            for tEvent in tEvents.midpoint ?? [] {
                trackingEvents.append(VastTrackingEvent(urlStr: tEvent, type: .midpoint, isTracked: false))
            }
            for tEvent in tEvents.thirdQuartile ?? [] {
                trackingEvents.append(VastTrackingEvent(urlStr: tEvent, type: .thirdQuartile, isTracked: false))
            }
            for tEvent in tEvents.complete ?? [] {
                trackingEvents.append(VastTrackingEvent(urlStr: tEvent, type: .complete, isTracked: false))
            }
        }
        
        return MMSSAIAdInfo(aId: model.titleId ?? "", aTitle: model.title ?? "", aServer: "", aIndex: self.adTimeLine.count + 1, aDuration: u_long(model.duration!/1000), sTime: startTime, eTime: (startTime + (model.duration ?? 0)), fqTime: (startTime + UInt(0.25)*(model.duration ?? 0)), mpTime: (startTime + UInt(0.5)*(model.duration ?? 0)), tqTime: (startTime + UInt(0.75)*(model.duration ?? 0)), complete: (startTime + (model.duration ?? 0)), adCurrentTime: 0, position: position, active: false, aState: "READY", sType: "hls", isLinear: true, atInfo: AdTrackerInfo(), aImpressionsTemplates: impressions, aTrackingEvents: trackingEvents)
    }
    
    private func syncPDTPosition(pdtInMs: u_long) {
        if programDateTimeAtSyncMs != pdtInMs{
            playPositionAtSyncMs = self.currentPlayerPosInMs
            programDateTimeAtSyncMs = pdtInMs
            if self.currentPlayerPosInMs > 0{
                livePlayPositionDiffAtSyncMs = u_long(fabs(Double((pdtInMs - self.currentPlayerPosInMs))))
            }else {
                livePlayPositionDiffAtSyncMs = pdtInMs
            }
        }
    }
    
    private func getSyncPosInMs(currentPosInMs: u_long) -> u_long {
        self.currentPlayerPosInMs = currentPosInMs
        if isLive {
            return (currentPosInMs + livePlayPositionDiffAtSyncMs);
        }
        return currentPosInMs
    }
        
    //MARK:- PARSE VAST URL
    @objc private func parseVASTURL() {
        let request = URLRequest(url: URL(string: self.stringVastURL)!)
        let session = URLSession(configuration: URLSessionConfiguration.default)

        let task = session.dataTask(with: request) {(data, response, error) in
            DispatchQueue.global().async {
                if error != nil || data == nil {
                    print("Client error!")
                    return
                }

                guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                    print("Server error!")
                    return
                }

//                guard let mime = response.mimeType, mime == "application/json" else {
//                    print("Wrong MIME type!")
////                    return
//                }
                
                guard let count = data?.count, count > 0 else {
                    return
                }
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                        let jsonDict = json
                        if let adStartTime = jsonDict["time"] as? NSNumber {
                            if self.currentAdStartTime == Int(truncating: adStartTime) {
                                return
                            }
                            self.currentAdStartTime = Int(truncating: adStartTime)
                            self.adIndex = 0
                        }
                        if let dataString = jsonDict["vast"] as? String {
                            self.makeVastRequestWithXMLData(stringXMLData: dataString, startTimeInMs: u_long(self.currentAdStartTime))
                        }
                    } else if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]] {
                        
                    }
                } catch {
                    print("parseVASTURL JSON error: \(error.localizedDescription)")
                }
                print("parseVASTURL Completed request: \(request.debugDescription)")
            }
        }

        task.resume()
    }
        
    //MARK:- MAKE VAST REQUEST WITH XML DATA
    private func makeVastRequestWithXMLData(stringXMLData: String, startTimeInMs: u_long) {
        self.vastClient.parseVast(withContentsOf: stringXMLData) { (vastModel, error) in
            if let error = error as? VastError {
                switch error {
                case .invalidXMLDocument:
                    print("Error: Invalid XML document")
                case .invalidVASTDocument:
                    print("Error: Invalid Vast Document")
                case .unableToCreateXMLParser:
                    print("Error: Unable to Create XML Parser")
                case .unableToParseDocument:
                    print("Error: Unable to Parse Vast Document")
                default:
                    print("Error: unexpected error ...")
                }
                return
            }
                
            guard let vastModel = vastModel else {
                print("Error: unexpected error ...")
                return
            }
            var adStartTimeInMs = startTimeInMs
            self.adTimeLine.removeAll()
            
            if(vastModel.ads.count > 0){
                for (index, ad) in vastModel.ads.enumerated() {
                    let adImpressionTemplates = ad.impressions
                    let adTrackingEvents = ad.creatives[0].linear?.trackingEvents
                    let adDurationInMs = (ad.creatives[0].linear?.duration ?? 0) * 1000
                    let fqtime = (adStartTimeInMs + u_long(adDurationInMs * 0.25))
                    let mptime = (adStartTimeInMs + u_long(adDurationInMs * 0.50))
                    let tqtime = (adStartTimeInMs + u_long(adDurationInMs * 0.75))
                    
                    let adInfo = MMSSAIAdInfo (aId: ad.id, aTitle: ad.adTitle ?? "", aServer: ad.adSystem?.system ?? "nowtilus", aIndex: index, aDuration: u_long(adDurationInMs)/1000, sTime: adStartTimeInMs, eTime: (adStartTimeInMs + UInt(adDurationInMs)), fqTime: fqtime, mpTime: mptime, tqTime: tqtime, complete: 0, adCurrentTime: 0, position: "MID", active: false, aState: "READY", sType: "hls", isLinear: true, atInfo: AdTrackerInfo(),aImpressionsTemplates:adImpressionTemplates, aTrackingEvents: adTrackingEvents ?? [] )
                    
                    // Add the new ad into adTimeline
                    self.adTimeLine.append(adInfo)
                    adStartTimeInMs += UInt(adDurationInMs)
                }
                // TODO
                // We need to trigger CueTimeline Point Added delegate
                print("new ad updated into adTimeline \(startTimeInMs)")
            }
            
        }
    }
        
    //MARK:- TRACK VAST AD
    private func trackVastAd(from vastModel: VastModel) {
        self.vastTracker = VastTracker(vastModel: vastModel, startTime: 0, supportAdBuffets: true, delegate: self, trackProgressCumulatively: true)
    }
        
    //MARK:- TIMER FUNCTIONS
    private func activateTimer() {
        if self.timeDelayToCheckVastResponse > 0.0 {
            self.timerGetVastResponse = Timer.scheduledTimer(timeInterval: self.timeDelayToCheckVastResponse, target: self, selector: #selector(self.parseVASTURL), userInfo: nil, repeats: true)
            self.timerGetVastResponse!.fire()
        }
    }
    private func invalidateTimer() {
        if let timer = self.timerGetVastResponse {
            timer.invalidate()
        }
    }
}

//MARK:- VAST TRACKER DELEGATE METHODS
extension MMSSAIAdManager: VastTrackerDelegate {
    func adBreakStart(vastTracker: VastTracker) {
//        self.delegate?.notifyAdEventWith(state: MMAdState.AD_BREAK_STARTED, vastAd: nil, vastTracker: vastTracker, andAdIndex: 0)
    }
    
    func adStart(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: MMAdState.AD_STARTED, adInfo: adInfo)
    }
    
    func adFirstQuartile(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: .AD_FIRST_QUARTILE, adInfo: adInfo)
    }
    
    func adMidPoint(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: .AD_MIDPOINT, adInfo: adInfo)
    }
    
    func adThirdQuartile(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: .AD_THIRD_QUARTILE, adInfo: adInfo)
    }
    
    func adComplete(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: .AD_COMPLETED, adInfo: adInfo)
    }
    
    func adImpression(adInfo: MMSSAIAdInfo) {
        self.delegate?.notifySSAIAdEventWith(state: MMAdState.AD_IMPRESSION, adInfo: adInfo)
    }
    
    
    func adStart(vastTracker: VastTracker, ad: VastAd) {
        let duration = ad.creatives.first?.linear?.duration
        self.currentAdEndTime = Int(playhead) + Int((duration ?? 0)) * 1000
        
        if !self.isAdRequested {
            self.delegate?.notifyAdEventWith(state: MMAdState.AD_REQUEST, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
            self.isAdRequested = true
        }
        
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_IMPRESSION, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
        
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_STARTED, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adFirstQuartile(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_FIRST_QUARTILE, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adMidpoint(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_MIDPOINT, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adThirdQuartile(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_THIRD_QUARTILE, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adComplete(vastTracker: VastTracker, ad: VastAd) {
        self.delegate?.notifyAdEventWith(state: MMAdState.AD_COMPLETED, vastAd: ad, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
    
    func adBreakComplete(vastTracker: VastTracker) {
//        self.delegate?.notifyAdEventWith(state: MMAdState.AD_BREAK_COMPLETED, vastAd: nil, vastTracker: vastTracker, andAdIndex: self.adIndex)
    }
}
