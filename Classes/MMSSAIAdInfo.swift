//
//  MMSSAIAdInfo.swift
//  VOPlayerDemoSwift
//
//  Created by Anurag Singh on 4/2/21.
//  Copyright © 2021 VOPlayer. All rights reserved.
//

import Foundation

struct MMSSAIAdInfo {
    
    var adId: String = ""
    var adTitle: String = ""
    var adServer: String = ""
    var adIndex: Int = 0
    var adDuration: u_long = 0
    var startTime: u_long = 0
    var endTime: u_long = 0
    var firstQuartile: u_long = 0
    var midPoint: u_long = 0
    var thirdQuartile: u_long = 0
    var complete: u_long = 0
    var adCurrentPlaybackTimeInSec: u_long = 0
    var position: String = ""
    var active: Bool = false
    var adState: String = ""
    var streamType: String = ""
    var isLinear: Bool = false
    var adTrackerInfo: AdTrackerInfo?
    var adImpressionsTemplates: [VastImpression]=[]
    var adTrackingEvents: [VastTrackingEvent]=[]
    
    init(aId: String, aTitle: String, aServer: String, aIndex: Int, aDuration: u_long, sTime: u_long, eTime: u_long, fqTime: u_long, mpTime: u_long, tqTime: u_long, complete: u_long, adCurrentTime: u_long, position: String, active: Bool, aState: String, sType: String, isLinear: Bool, atInfo: AdTrackerInfo, aImpressionsTemplates:[VastImpression], aTrackingEvents: [VastTrackingEvent]) {
        self.adId = aId
        self.adTitle = aTitle
        self.adServer = aServer
        self.adIndex = aIndex
        self.adDuration = aDuration
        self.startTime = sTime
        self.endTime = eTime
        self.firstQuartile = fqTime
        self.midPoint = mpTime
        self.thirdQuartile = tqTime
        self.complete = complete
        self.adCurrentPlaybackTimeInSec = adCurrentTime
        self.position = position
        self.active = active
        self.adState = aState
        self.streamType = sType
        self.isLinear = isLinear
        self.adTrackerInfo = atInfo
        self.adImpressionsTemplates = aImpressionsTemplates
        self.adTrackingEvents = aTrackingEvents
    }
    
    mutating func updateAdState(state: String) {
        self.adState = state
    }
}

struct AdTrackerInfo {
    
    var isAdImpressionSent: Bool = false
    var isAdStartSent: Bool = false
    var isFirstQuartileSent = false
    var isMidPointSent = false
    var isThirdQuartileSent = false
    var isAdCompleteSent = false
    
    mutating func resetToDefault() {
        isAdImpressionSent = false
        isAdStartSent = false
        isFirstQuartileSent = false
        isMidPointSent = false
        isThirdQuartileSent = false
        isAdCompleteSent = false
    }
    
    mutating func setAdComplete() {
        isAdImpressionSent = false;
        isAdStartSent = false;
        isFirstQuartileSent = false;
        isMidPointSent = false;
        isThirdQuartileSent = false;
        isAdCompleteSent = true;
    }
}

/*
 public boolean isAdImpressionSent;
   public boolean isAdStartSent;
   public boolean isFirstQuartileSent;
   public boolean isMidQuartileSent;
   public boolean isThirdQuartileSent;
   public boolean isAdCompleteSent;
 
 
 public String adId;
   public String adCreativeId;
   public String adTitle;
   public String adServer;
   public int adIndex;
   public long adDuration;
   public long startTime;
   public long endTime;
   public long firstQuartile;
   public long midPoint;
   public long thirdQuartile;
   public long adCurrentPlaybackTimeInSec;
   public String position;
   public boolean active;
   public String adState;
   public String streamType;
   public boolean isLinear;
   public mmAdTrackerInfo adTrackerInfo;
   public JSONObject adInfo;

   public MMVastParser trackerObj;

 */
