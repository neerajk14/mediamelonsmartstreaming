//
//  ReachabilityManager.swift
//  AllInOneDemoPlayer
//
//  Created by MacAir 1 on 08/10/20.
//  Copyright © 2020 FreeWheel. All rights reserved.
//

import UIKit
import Foundation
#if os(iOS)
  import CoreTelephony
#endif


@objc public class ReachabilityManager: NSObject {
    //MARK:- OBJECTS
    public static  let shared = ReachabilityManager()
    let reachability: ReachabilityMM = ReachabilityMM()!
    var reachabilityStatus: ReachabilityMM.NetworkStatus = .notReachable
    
    //MARK:- METHODS
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! ReachabilityMM
        var connInfo: MMConnectionInfo!
        
        #if os(iOS)
            func getDetailedMobileNetworkType() {
                let phoneInfo = CTTelephonyNetworkInfo()
                if #available(iOS 12.0, *) {
                    if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                        var networkString = ""
                        for value in arrayNetworks.values {
                            networkString = value
                        }
                        if networkString == CTRadioAccessTechnologyLTE{
                            connInfo = .cellular_4G
                        }else if networkString == CTRadioAccessTechnologyWCDMA{
                            connInfo = .cellular_3G
                        }else if networkString == CTRadioAccessTechnologyEdge{
                            connInfo = .cellular_2G
                        }
                    }
                } else {
                    let networkString = phoneInfo.currentRadioAccessTechnology
                    if networkString == CTRadioAccessTechnologyLTE{
                        connInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connInfo = .cellular_2G
                    }
                }
            }
        #endif
        
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            connInfo = .notReachable
        case .reachableViaWiFi:
            connInfo = .wifi
        case .reachableViaWWAN:
            connInfo = .cellular
            #if os(iOS)
                getDetailedMobileNetworkType()
            #endif
        }
        if (connInfo != nil) {
            AVPlayerIntegrationWrapper.shared.reportNetworkType(connInfo:connInfo)
        }
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotificationMM,
                                               object: self.reachability)
        do{
            try self.reachability.startNotifier()
        } catch {
        }
    }
    
    func stopMonitoring(){
        self.reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotificationMM,
                                                  object: self.reachability)
    }
}
