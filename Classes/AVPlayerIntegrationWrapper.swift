import Foundation
import UIKit
import AVFoundation
import os
#if os(iOS)
  import CoreTelephony
#endif

private var AVFoundationPlayerViewControllerKVOContext = 0
private var AVFoundationPlayerViewControllerKVOContextPlayer = 0

@objc public class AVPlayerIntegrationWrapper: NSObject, MMSmartStreamingObserver {
    //MARK:- OBJECTS
    private enum CurrentPlayerState {
        case IDLE,
        PLAYING,
        PAUSED,
        STOPPED,
        ERROR
    }
    
    private var sessTerminated: Bool = false
    private var timer: Timer?
    private var timerFWAd: Timer?
    weak private var player: AVPlayer?
    weak private var playerItemObserverAdded: AVPlayerItem?
        
    private let TIME_INCREMENT = 1.0
    private var presentationInfoSet = false
    private var infoEventIdx = 0
    private var errEventIdx = 0
    private var infoEventsIdxToSkip = -1
    private var errorEventsIdxToSkip = -1
    private var durationWatchedTotal: TimeInterval = 0
    private var currentBitrate: Double = 0
    private var frameLossCnt: Int = 0
    private var isInitialBitrateReported = false;
    private var lastObservedBitrateOfContinuedSession = -1.0
    private var lastObservedDownlaodRateOfContinuedSession = -1
    private var contentURL: String?
    private var currentState = CurrentPlayerState.IDLE
    private var lastPlaybackPos: Int64 = 0
    private var lastPlaybackPosRecordTime: Int64 = 0
    private var notificationObservors = [Any]()
    private var timeObservor: Any?
    private var playerObserved:Bool = false
    private var assetInfo: MMAssetInformation?
    private var sessionInStartedState = false
    private var loadEventTriggered = false
    private static var enableLogging = false
    
    private enum AVPlayerPropertiesToObserve: String {
        case PlaybackRate = "rate",
        CurrentItem = "currentItem"
    }
    
    private enum AVPlayerItemPropertiesToObserve: String {
        case Duration = "duration",
        Playable = "playable",
        ItemStatus = "status",
        PresentationSize = "presentationSize",
        PlaybackBufferEmpty = "playbackBufferEmpty",
        PlaybackLikelyToKeepUp = "playbackLikelyToKeepUp"
    }
    
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private static let KPlaybackSessionID = "PlaybackSessionID"
    private static let KErrorInstantTime = "ErrorInstantTime"
    private static let KPlaybackUri = "PlaybackUri"
    private static let KErrorStatusCode = "StatusCode"
    private static let KServerAddress = "ServerAddress"
    private static let KErrorDomain = "ErrorDomain"
    private static let KErrorComment = "ErrorComment"
    private static let KPlaybackPosPollingIntervalSec = 0.5
    private static let KPlaybackPosPollingIntervalMSec = 500
    private static let KMinPlaybackPosDriftForPlayingStateMSec = 400
    private static let KMinPlaybackPosDriftForPausedStateMSec = 200
    private static var appNotificationObsRegistered = false
    private var presentationDuration:CMTime?
    /*
     * Singleton instance of the adaptor
     */
    public static  let shared = AVPlayerIntegrationWrapper()
    
    //MARK:- METHODS
    /**
     * Gets the version of the SDK
     */
    @objc public static func getVersion() -> String! {
        return "4.0.0/\(String(describing: MMSmartStreaming.getVersion()!))"
    }
    
    /**
     * If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
     * For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
     * So that player can fetch the manifest
     */
    @objc public static func disableManifestsFetch(disable: Bool) {
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    /**
     * Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
     * Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
     *
     * Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
     *
     * This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
     */
    public static func setPlayerRegistrationInformation(registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: AVPlayer?) {
        if let pInfo = pInfo {
            AVPlayerIntegrationWrapper.logDebugStatement("=============setPlayerInformation - pInfo=============")
            #if os(iOS)
                pInfo.setComponentName("IOSSDK")
            #elseif os(tvOS)
                pInfo.setComponentName("tvOSSDK")
            #endif
            
            AVPlayerIntegrationWrapper.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component, subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
            AVPlayerIntegrationWrapper.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
        }
        if let oldPlayer = AVPlayerIntegrationWrapper.shared.player {
            AVPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
        }
        if let newPlayer = aPlayer {
            AVPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
        }
    }
    
    /**
     * Application may create the player with the AVAsset for every session they do the playback
     * User of API must provide the asset Information
     */
    @objc public static func initializeAssetForPlayer(assetInfo aInfo: MMAssetInformation, registrationInformation pInfo: MMRegistrationInformation?, player aPlayer: AVPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        AVPlayerIntegrationWrapper.logDebugStatement("================initializeAssetForPlayer \(aInfo.assetURL)=============")
        AVPlayerIntegrationWrapper.setPlayerRegistrationInformation(registrationInformation: pInfo, player:aPlayer)
        AVPlayerIntegrationWrapper.changeAssetForPlayer(assetInfo: aInfo, player: aPlayer)
    }
    
    /**
     * Whenever the asset with the player is changed, user of the API may call this API
     * Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
     */
    public static func changeAssetForPlayer(assetInfo aInfo: MMAssetInformation, player aPlayer: AVPlayer?) {
        AVPlayerIntegrationWrapper.logDebugStatement("================changeAssetForPlayer \(aInfo.assetURL)=============")
        AVPlayerIntegrationWrapper.shared.assetInfo = aInfo
        AVPlayerIntegrationWrapper.shared.cleanupCurrItem();
        
        if let newPlayer = aPlayer {
            if let oldPlayer = AVPlayerIntegrationWrapper.shared.player {
                if oldPlayer != newPlayer {
                    AVPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
                    AVPlayerIntegrationWrapper.shared.player = nil;
                    AVPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
                }
                
                if let playerItem = newPlayer.currentItem {
                    AVPlayerIntegrationWrapper.shared.initSession(player: newPlayer, playerItem: playerItem , deep: true)
                }
            }
        }
    }
    
    /**
     * Once the player is done with the playback session, then application should call this API to clean up observors set with the player and the player's current item
     */
    @objc public static func cleanUp() {
        AVPlayerIntegrationWrapper.logDebugStatement("================cleanUp=============")
        AVPlayerIntegrationWrapper.shared.cleanupInstance()
    }
    
    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId: String!, subscriberType: String!, subscriberMetadata: String!) {
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }
    
    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAVAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key: String!, value: String!) {
        self.mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }
    
    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace: Bool) {
        AVPlayerIntegrationWrapper.enableLogging = logStTrace
        self.mmSmartStreaming.enableLogTrace(logStTrace)
    }
    
    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error: String, playbackPosMilliSec: Int) {
        self.mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }
    
    public static func reportMetricValue(metricToOverride: MMOverridableMetrics, value: String!) {
        switch metricToOverride {
        case MMOverridableMetrics.CDN:
            AVPlayerIntegrationWrapper.shared.mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
        default:
            print("Only CDN metric is overridable as of now ...")
        }
    }
    
    private static func logDebugStatement(_ logStatement: String) {
        if(AVPlayerIntegrationWrapper.enableLogging) {
            //os_log("mediamelon.smartstreaming.avplayer-ios %{public}@", logStatement)
            NSLog("%s", logStatement)
        }
    }
    
    private static func registerMMSmartStreaming(playerName: String, custID: String, component: String,  subscriberID: String?, domainName: String?, subscriberType: String?, subscriberTag: String?) {
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag);
        
        var operatorName = ""
        var osName = ""
        var osVersion = UIDevice.current.systemVersion
        let brand = "Apple"
        let model = UIDevice.current.model
        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)
        
        #if os(iOS)
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let carrier = phoneInfo.serviceSubscriberCellularProviders, let dict = carrier.first, let opName = dict.value.carrierName {
                    operatorName = opName
                }
            }
            else {
                if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName{
                    operatorName = carrierName
                }
            }
            osName = "iOS"
            MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
        #elseif os(tvOS)
            osName = "tvOS"
            MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType:"AppleTV")
        #endif
        
    }

    private static func reportPlayerInfo(brand: String?, model: String?, version: String?) {
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }
    
    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetID: String?, assetName: String?, videoId: String?) {
        self.contentURL = manifestURL
        var connectionInfo: MMConnectionInfo!
        let reachability = ReachabilityMM()
        
        #if os(iOS)
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        #endif
        
        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    #if os(iOS)
                        getDetailedMobileNetworkType()
                    #endif
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }
    
    
    private func initializeSession(mode: MMQBRMode!, manifestURL: String!, metaURL: String?, assetInfo: MMAssetInformation) {
        self.contentURL = manifestURL
        var connectionInfo: MMConnectionInfo!
        let reachability = ReachabilityMM()
        
        #if os(iOS)
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        #endif
        
        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    #if os(iOS)
                        getDetailedMobileNetworkType()
                    #endif
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            self.mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        let contentMetadata = MMContentMetadata()
        contentMetadata.assetId = assetInfo.assetID
        contentMetadata.assetName = assetInfo.assetName
        contentMetadata.videoId = assetInfo.videoId
        contentMetadata.contentType = assetInfo.contentType
        contentMetadata.genre = assetInfo.genre
        contentMetadata.season = assetInfo.season
        contentMetadata.drmProtection = assetInfo.drmProtection
        contentMetadata.seriesTitle = assetInfo.seriesTitle
        contentMetadata.episodeNumber = assetInfo.episodeNumber
        
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, contentMetadata: contentMetadata, for: self)
    }
    
    func reportNetworkType(connInfo: MMConnectionInfo) {
        self.mmSmartStreaming.reportNetworkType(connInfo)
    }

    private func reportLocation(latitude: Double, longitude: Double) {
        self.mmSmartStreaming.reportLocation(withLatitude: latitude, andLongitude: longitude)
    }
    
    public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String?, forCmdWithId cmdId: Int) {
        AVPlayerIntegrationWrapper.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }
    
    private func reportChunkRequest(chunkInfo: MMChunkInformation!) {
        self.mmSmartStreaming.reportChunkRequest(chunkInfo)
    }
    
    private func setPresentationInformation(presentationInfo: MMPresentationInfo!) {
        self.mmSmartStreaming.setPresentationInformation(presentationInfo)
    }
    
    private func reportDownloadRate(downloadRate: Int!) {
        self.mmSmartStreaming.reportDownloadRate(downloadRate)
    }
    
    private func reportBufferingStarted() {
        self.mmSmartStreaming.reportBufferingStarted()
    }
    
    private func reportBufferingCompleted() {
        self.mmSmartStreaming.reportBufferingCompleted()
    }
    
    private func reportABRSwitch(prevBitrate: Int, newBitrate: Int) {
        self.mmSmartStreaming.reportABRSwitch(fromBitrate: prevBitrate, toBitrate: newBitrate)
    }
    
    private func reportFrameLoss(lossCnt: Int) {
        self.mmSmartStreaming.reportFrameLoss(lossCnt)
    }
    
    @objc private func timeoutOccurred() {
        guard self.player != nil else {
            return
        }
        
        self.mmSmartStreaming.reportPlaybackPosition(getPlaybackPosition())
    }
    
    private func logIntegration(str: String) {
        
    }
    
    private func reportPresentationSize(width: Int, height: Int) {
        self.mmSmartStreaming.reportPresentationSize(withWidth: width, andHeight: height)
    }
    
    private func getPlaybackPosition() -> Int {
        guard let player = self.player else{
            return 0
        }
        let time = player.currentTime()
        if(time.timescale > 0){
            return Int((time.value)/(Int64)(time.timescale)) * 1000;
        }else{ //Avoid dividing by 0 , -ve should not be expected
            return 0
        }
    }
    
    private func cleanupInstance() {
        self.cleanupCurrItem()
        self.cleanupSession(player: self.player)
    }
    
    private func cleanupCurrItem() {
        guard let player = self.player else{
            return
        }
        guard let playerItem = player.currentItem else {
            return
        }
        self.resetSession(item: playerItem)
    }
    
    private func playbackDidReachEnd(notification noti: Notification) {
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackDidReachEnd ---")
        self.cleanupCurrItem()
    }
    
    private func playbackFailedToPlayTillEnd(notification noti: Notification) {
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackFailedToPlayTillEnd ---")
        self.mmSmartStreaming.reportError(noti.description, atPosition: -1)
        self.currentState = .ERROR
        self.cleanupCurrItem()
    }
    
    private func handleErrorWithMessage(message: String?, error: Error? = nil) {
        AVPlayerIntegrationWrapper.logDebugStatement("--- Error occurred with message: \(String(describing: message)), error: \(String(describing: error)).")
        self.mmSmartStreaming.reportError(String(describing: error), atPosition: getPlaybackPosition())
    }
    
    private func reset() {
        self.lastPlaybackPos = 0
        self.lastPlaybackPosRecordTime = 0
        self.sessionInStartedState = false
        self.presentationInfoSet = false
        self.infoEventIdx = 0
        self.errEventIdx = 0
        self.infoEventsIdxToSkip = -1
        self.errorEventsIdxToSkip = -1
        self.lastObservedBitrateOfContinuedSession = -1.0
        self.lastObservedDownlaodRateOfContinuedSession = -1
        self.durationWatchedTotal = 0
        self.currentBitrate = 0
        self.frameLossCnt = 0
        self.isInitialBitrateReported = false
        self.contentURL = nil
        self.currentState = CurrentPlayerState.IDLE
        self.loadEventTriggered = false
        
        if AVPlayerIntegrationWrapper.appNotificationObsRegistered == false{
            AVPlayerIntegrationWrapper.appNotificationObsRegistered = true
            
            NotificationCenter.default.addObserver(
                forName: NSNotification.Name.UIApplicationWillResignActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.stopMonitoring()
            }
            
            NotificationCenter.default.addObserver(
                forName: NSNotification.Name.UIApplicationDidBecomeActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.startMonitoring()
            }
            ReachabilityManager.shared.startMonitoring()
        }
    }
    
    private func createSession(player: AVPlayer) {
        AVPlayerIntegrationWrapper.logDebugStatement("*** createSession")
        self.player = player
        self.addPlayerObservors();
        self.timer = nil
    }
    
    private func initSession(player: AVPlayer, playerItem: AVPlayerItem, deep: Bool) {
        AVPlayerIntegrationWrapper.logDebugStatement("*** initSession")
        var infoEventsIdxToSkipCache = -1
        var errorEventsIdxToSkipCache = -1
        
        if (self.infoEventsIdxToSkip != -1){
            infoEventsIdxToSkipCache = self.infoEventsIdxToSkip
        }
        if (self.errorEventsIdxToSkip != -1){
            errorEventsIdxToSkipCache = self.errorEventsIdxToSkip
        }
        
        if(deep){
            self.reset()
        }
        
        self.infoEventsIdxToSkip = infoEventsIdxToSkipCache
        self.errorEventsIdxToSkip = errorEventsIdxToSkipCache
        
        guard let assetInfo = self.assetInfo else {
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - assetInfo not set !!!")
            return
        }
        
        self.sessTerminated = false;
        
        if(deep) {
            AVPlayerIntegrationWrapper.shared.initializeSession(mode: assetInfo.qbrMode, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL? .absoluteString, assetInfo: assetInfo)
            for (key, value) in assetInfo.customKVPs {
                AVPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
            AVPlayerIntegrationWrapper.shared.reportUserInitiatedPlayback();
        } else {
            for (key, value) in assetInfo.customKVPs {
                AVPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: self.TIME_INCREMENT, target:self, selector:#selector(AVPlayerIntegrationWrapper.timeoutOccurred), userInfo: nil, repeats: true)
        
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Duration.rawValue, options: [.new], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Playable.rawValue, options: [.new], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.ItemStatus.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PresentationSize.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        
        var observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem, queue: nil, using: {  (not) in  self.playbackDidReachEnd(notification:not)})
        self.notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: playerItem, queue: nil, using: {  (not) in  self.playbackFailedToPlayTillEnd(notification:not)})
        self.notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: playerItem, queue: nil, using: {  (not) in  self.newAccessLogEntryRecvd(notification:not)})
        self.notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: playerItem, queue: nil, using: {  (not) in  self.newErrorLogEntryRecvd(notification:not)})
        self.notificationObservors.append(observor)
        
        self.playerItemObserverAdded = playerItem
        if (self.infoEventsIdxToSkip >= 0 || self.errorEventsIdxToSkip >= 0) {
            self.processEventsRegister()
            self.processErrorEventsRegister()
            self.infoEventsIdxToSkip = -1
            self.errorEventsIdxToSkip = -1
        }
        
        let url = (playerItem.asset as? AVURLAsset)?.url
        AVPlayerIntegrationWrapper.logDebugStatement("Initializing for \(String(describing: url))")
    }
    
    private func resetSession(item: AVPlayerItem?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        guard let playerItem = item else {
            return;
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == item) {
            for item in self.notificationObservors {
                NotificationCenter.default.removeObserver(item);
            }
            self.playerItemObserverAdded = nil
            self.notificationObservors.removeAll();

            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Duration.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Playable.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.ItemStatus.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PresentationSize.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);

            if let tmr = self.timer {
                tmr.invalidate();
            }
            
            self.reportStoppedState()
            self.sessTerminated = true;
            let url = (playerItem.asset as? AVURLAsset)?.url
            AVPlayerIntegrationWrapper.logDebugStatement("resetSession \(String(describing: url)) ***")
        }
    }
    
    private func cleanupSession(player: AVPlayer?) {
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if let player = player {
            self.removePlayerObservors(player: player);
        }
        self.player = nil
        AVPlayerIntegrationWrapper.logDebugStatement("removeSession ***")
    }
    
    private func continueStoppedSession() {
        guard let player = self.player else{
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, player not avl")
            return
        }
        guard let playerItem = self.player?.currentItem else {
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, playerItem not avl")
            return
        }
        self.sessTerminated = false;
        //Lets save the player events that were sent earlier. The ones those were pushed earlier, will reappear after deep init of session.
        self.infoEventsIdxToSkip = infoEventIdx
        self.errorEventsIdxToSkip = errEventIdx
        self.initSession(player: player, playerItem: playerItem, deep: true)
        self.setPresentationInformationForContent()
    }
    
    private func reportStoppedState() {
        if(self.currentState != .IDLE && self.currentState != .STOPPED) {
            if let player = self.player {
                let time = player.currentTime();
                if(time.timescale > 0){
                    self.mmSmartStreaming.reportPlaybackPosition(Int((time.value)/(Int64)(time.timescale)) * 1000)
                }
            }
            self.mmSmartStreaming.report(.STOPPED)
            self.currentState = .STOPPED
        }
    }
    
    private func reportUserInitiatedPlayback() {
        self.mmSmartStreaming.reportUserInitiatedPlayback()
    }
    
    private func reportPlayerSeekCompleted(seekEndPos: Int) {
        self.mmSmartStreaming.reportPlayerSeekCompleted(seekEndPos)
    }
    
    private func processDuration(change: [NSKeyValueChangeKey : Any]?) {
        guard self.player != nil else {
            return;
        }

        let newDuration: CMTime
        if let newDurationAsValue = change?[NSKeyValueChangeKey.newKey] as? NSValue {
            newDuration = newDurationAsValue.timeValue
        }
        else {
            newDuration = kCMTimeZero
        }
        
        self.presentationDuration = newDuration
        self.setPresentationInformationForContent()
    }
    
    private func processDurationFromPlayerItem() {
        guard self.player != nil else {
            return;
        }
        
        if let item = self.player?.currentItem {
            self.presentationDuration = item.duration
            self.setPresentationInformationForContent()
        }
    }
    
    private func setPresentationInformationForContent() {
        guard let contentDuration = presentationDuration else{
            return
        }

        if !self.presentationInfoSet {
            let presentationInfo = MMPresentationInfo()
            let hasValidDuration = contentDuration.isNumeric && contentDuration.value != 0
            if(hasValidDuration){
                let newDurationSeconds = hasValidDuration ? CMTimeGetSeconds(contentDuration) : 0.0
                AVPlayerIntegrationWrapper.logDebugStatement("Duration of content is \(String(describing: newDurationSeconds * 1000))")
                let duration  = newDurationSeconds * 1000
                presentationInfo.duration = Int(duration)
            } else {
                presentationInfo.duration = Int(-1)
                presentationInfo.isLive = true
            }
            self.mmSmartStreaming.setPresentationInformation(presentationInfo)
            self.presentationInfoSet = true
        }
    }
    
    private func processPlaybackRate(_ rate:Float){
        switch rate {
        case 0.0:
            AVPlayerIntegrationWrapper.logDebugStatement("LATENCY: - Playback rate 0")
            if self.currentState != .PAUSED{
                self.mmSmartStreaming.report(MMPlayerState.PAUSED);
                self.currentState = .PAUSED
            }

        default:
            AVPlayerIntegrationWrapper.logDebugStatement("LATENCY: - Playback rate \(rate)")
        }
    }
    
    private func playbackFailed(){
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackFailed ---")
        var currContent = "Content Not Set";
        if let contentURL = self.contentURL {
            currContent = contentURL;
        }
        self.mmSmartStreaming.reportError(String("Playback of \(currContent) Failed"), atPosition: self.getPlaybackPosition())
        self.cleanupCurrItem();
    }
    
    private func processCurrentItemChange(old oldItem:AVPlayerItem?, new newItem:AVPlayerItem?){
        guard let player = self.player else{
            return
        }
        AVPlayerIntegrationWrapper.logDebugStatement("--Process current item changed with the player--")
        // Player Item has changed (asset being played)
        if let oldItem = oldItem{
            self.resetSession(item: oldItem)
        }

        if let newItem = newItem{
            self.initSession(player: player, playerItem: newItem, deep: true)
        }
    }
    
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &AVFoundationPlayerViewControllerKVOContext || context == &AVFoundationPlayerViewControllerKVOContextPlayer else {
            super.observeValue(forKeyPath:keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == AVPlayerItemPropertiesToObserve.Duration.rawValue {
            self.processDuration(change: change)
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.ItemStatus.rawValue {
            let newStatus: AVPlayerItem.Status
            if let newStatusAsNumber = change?[NSKeyValueChangeKey.newKey] as? NSNumber {
                newStatus = AVPlayerItem.Status(rawValue: newStatusAsNumber.intValue)!
                if newStatus == .failed{
                    self.playbackFailed();
                }
            }
            else {
                newStatus = .unknown
            }
            
            switch newStatus {
            case .readyToPlay:
                self.processDurationFromPlayerItem()
            default:
                print("")
            }
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue {
            self.mmSmartStreaming.reportBufferingStarted()
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue { //Change to handle the event: kCMTimebaseNotification_EffectiveRateChanged for buffering completion
            self.mmSmartStreaming.reportBufferingCompleted()
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PresentationSize.rawValue {
            if let presentationSz = change?[NSKeyValueChangeKey.newKey]{
                let pSz = presentationSz as! CGSize
                self.mmSmartStreaming.reportPresentationSize(withWidth: Int(pSz.width), andHeight: Int(pSz.height))
            }
        }
        else if keyPath ==  AVPlayerPropertiesToObserve.PlaybackRate.rawValue{
            self.processPlaybackRate((object! as AnyObject).rate);
        }
        else if keyPath == AVPlayerPropertiesToObserve.CurrentItem.rawValue{
            let newItem = change?[NSKeyValueChangeKey.newKey] as? AVPlayerItem
            let oldItem = change?[NSKeyValueChangeKey.oldKey] as? AVPlayerItem
            self.processCurrentItemChange(old:oldItem, new:newItem);
        }
    }
    
    private func addPlayerObservors(){
        guard let player = self.player else{
            return
        }
        
        player.addObserver(self, forKeyPath: AVPlayerPropertiesToObserve.PlaybackRate.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContextPlayer)
        player.addObserver(self, forKeyPath: AVPlayerPropertiesToObserve.CurrentItem.rawValue, options: [.new, .old], context: &AVFoundationPlayerViewControllerKVOContextPlayer)
        
        self.startSeekWatchdogAndPlaybackPositionTracker()
        self.playerObserved = true
    }
    
    private func removePlayerObservors(player:AVPlayer){
        if(self.playerObserved == true){
            if let timeObservor = self.timeObservor{
                player.removeTimeObserver(timeObservor)
            }
            self.timeObservor = nil
            player.removeObserver(self, forKeyPath: AVPlayerPropertiesToObserve.PlaybackRate.rawValue);
            player.removeObserver(self, forKeyPath: AVPlayerPropertiesToObserve.CurrentItem.rawValue);
            self.playerObserved = false;
        }
    }
    
    
    private func processEventsRegister(){
        guard let player = self.player else{
            return
        }
        
        guard let playerItem = player.currentItem else{
            return
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == playerItem) {
            if let evtCount = playerItem.accessLog()?.events.count {
                AVPlayerIntegrationWrapper.logDebugStatement("Total events \(String(describing: evtCount))")
                if (self.infoEventIdx > evtCount) {
                    self.errEventIdx = 0
                    self.infoEventIdx = 0
                    self.infoEventsIdxToSkip = -1
                    self.errorEventsIdxToSkip = -1
                }
                for i in stride(from: self.infoEventIdx, to: evtCount, by: 1) {
                    let accessEvt = playerItem.accessLog()?.events[i]
                    
                    if(self.infoEventsIdxToSkip >= self.infoEventIdx){ //We are deliberately letting download rate be passed in replay of prev session. Because, it may not be set again on replay ...
                        if let indicatedBitrate = accessEvt?.indicatedBitrate{
                            AVPlayerIntegrationWrapper.logDebugStatement("Skipping Event, indicatedBitrate \(indicatedBitrate)")
                            if  indicatedBitrate > 0.0{
                                self.lastObservedBitrateOfContinuedSession = indicatedBitrate
                            }
                        }
                        
                        if let obsBitrate = accessEvt?.observedBitrate{
                            if obsBitrate.isNormal{
                                self.lastObservedDownlaodRateOfContinuedSession = Int(obsBitrate)
                            }
                        }
                        
                        AVPlayerIntegrationWrapper.logDebugStatement("Skipping Event, Pending \(self.infoEventsIdxToSkip - self.infoEventIdx)")
                        self.infoEventIdx += 1;
                        continue
                    }
                    
                    if self.lastObservedBitrateOfContinuedSession > 0 {
                        self.currentBitrate = self.lastObservedBitrateOfContinuedSession
                        AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(currentBitrate) => \(currentBitrate)")
                        self.mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                        self.isInitialBitrateReported = true
                        self.lastObservedBitrateOfContinuedSession = -1.0
                    }
                    
                    if self.lastObservedDownlaodRateOfContinuedSession > 0 {
                        self.mmSmartStreaming.reportDownloadRate(self.lastObservedDownlaodRateOfContinuedSession)
                        self.lastObservedDownlaodRateOfContinuedSession = -1
                    }
                    
                    if let obsBitrate = accessEvt?.observedBitrate{
                        if obsBitrate.isNormal{
                            self.mmSmartStreaming.reportDownloadRate(Int(obsBitrate))
                        }
                    }
                    
                    if let indicatedBitrate = accessEvt?.indicatedBitrate{
                        if indicatedBitrate > 0.0{
                            if self.currentBitrate == 0{
                                self.currentBitrate = indicatedBitrate
                            }
                            
                            if(self.isInitialBitrateReported == false){
                                self.isInitialBitrateReported = true
                                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(self.currentBitrate) => \(self.currentBitrate)")
                                self.mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                            }else{
                                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: \(self.currentBitrate) => \(indicatedBitrate)")
                                if self.currentBitrate != indicatedBitrate{
                                    self.mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(indicatedBitrate))
                                }
                            }
                            self.currentBitrate = indicatedBitrate
                        }
                    }
                    
                    if let droppedFrames = accessEvt?.numberOfDroppedVideoFrames{
                        if droppedFrames > self.frameLossCnt{
                            self.mmSmartStreaming.reportFrameLoss((droppedFrames-frameLossCnt))
                            self.frameLossCnt = droppedFrames
                        }
                    }
                    self.infoEventIdx += 1;
                }
            }
            
            if self.lastObservedBitrateOfContinuedSession > 0 {
                self.currentBitrate = lastObservedBitrateOfContinuedSession
                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(currentBitrate) => \(currentBitrate)")
                self.mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                self.isInitialBitrateReported = true
                self.lastObservedBitrateOfContinuedSession = -1.0
            }
            
            if self.lastObservedDownlaodRateOfContinuedSession > 0 {
                self.mmSmartStreaming.reportDownloadRate(lastObservedDownlaodRateOfContinuedSession)
                self.lastObservedDownlaodRateOfContinuedSession = -1
            }
        }
    }
    
    private func newAccessLogEntryRecvd(notification noti:Notification){
        self.processEventsRegister();
    }
    
    private func processErrorEventsRegister() {
        guard let player = self.player else{
            return
        }
        
        guard let playerItem = player.currentItem else{
            return
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == playerItem) {
            if let errEvtCount = playerItem.errorLog()?.events.count {
                AVPlayerIntegrationWrapper.logDebugStatement("Total Err events \(String(describing: errEvtCount))")
                var kvpsForErr:[String] = []
                if (self.errEventIdx > errEvtCount) {
                    self.errEventIdx = 0
                    self.infoEventIdx = 0
                    self.infoEventsIdxToSkip = -1
                    self.errorEventsIdxToSkip = -1
                }
                for i in stride(from: self.errEventIdx, to: errEvtCount, by: 1) {
                    
                    if(self.errorEventsIdxToSkip >= self.errEventIdx){ //We are deliberately letting download rate be
                        
                        AVPlayerIntegrationWrapper.logDebugStatement("Skipping ERR Event, Pending \(self.errorEventsIdxToSkip - self.errEventIdx)")
                        self.errEventIdx += 1;
                        continue
                    }
                    
                    let errEvt = playerItem.errorLog()?.events[i]
                    if let sessID = errEvt?.playbackSessionID {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KPlaybackSessionID + "=" + sessID)
                    }
                    
                    if let time = errEvt?.date {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorInstantTime + "=" + time.description)
                    }
                    
                    if let uri = errEvt?.uri {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KPlaybackUri + "=" + uri)
                    }
                    
                    if let statusCode = errEvt?.errorStatusCode {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorStatusCode + "=" + String(statusCode))
                    }
                    
                    if let serverAddr = errEvt?.serverAddress {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KServerAddress + "=" + String(serverAddr))
                    }
                    
                    if let errDomain = errEvt?.errorDomain {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorDomain + "=" + errDomain)
                    }
                    
                    if let errComment = errEvt?.errorComment {
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorComment + "=" + errComment)
                    }
                    self.errEventIdx += 1
                }
                
                if kvpsForErr.count > 0 {
                    //Lets not distinguish errors, filterning can better be handled in backend
                    //Session termination can be handled via player notifications when it gives up
                    let errString  = kvpsForErr.joined(separator: ":")
                    self.mmSmartStreaming.reportError(errString, atPosition: getPlaybackPosition())
                }
            }
        }
    }
    
    private func newErrorLogEntryRecvd(notification noti: Notification) {
        self.processErrorEventsRegister()
    }
    
    private static func isPossibleSeek(currpos: Int64, lastPos: Int64, currentRecordTime: Int64, lastRecInstant:Int64) -> Bool {
        if lastRecInstant < 0 || lastPos<0 {
            return false
        }
        let posDiff = abs(currpos - lastPos)
        let wallclockTimeDiff = currentRecordTime - lastRecInstant
        let drift = Int(abs(wallclockTimeDiff - posDiff))
        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick isPossibleSeek: playDelta [\(posDiff) msec] wallclkDelta [\(wallclockTimeDiff) msec] drift [\(drift) msec]");
        
        if drift > AVPlayerIntegrationWrapper.KPlaybackPosPollingIntervalMSec {
            return true
        }
        return false
    }
    
    private func startSeekWatchdogAndPlaybackPositionTracker() {
        guard let player = self.player else {
            return
        }
        
        self.timeObservor = player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(AVPlayerIntegrationWrapper.KPlaybackPosPollingIntervalSec, Int32(NSEC_PER_SEC)), queue: nil){ [weak self] time in
            
            
            guard let player = self?.player else {
                return
            }
            
            guard let playerItem = player.currentItem else {
                return
            }
            
            let currentPlaybackPos = Int64(CMTimeGetSeconds(time) * 1000);
            let currentRecordTime = Int64(CFAbsoluteTimeGetCurrent() * 1000)
            AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Periodic Check -- pos=\(currentPlaybackPos) at=\(currentRecordTime)")
            if let timebase = playerItem.timebase {
                let observedRate = CMTimebaseGetRate(timebase)
                if observedRate > 0 {
                    AVPlayerIntegrationWrapper.logDebugStatement(" -- tick observed rate \(observedRate) player rate \(player.rate) --")
                    if player.rate != 0 && currentPlaybackPos > 100{ //Atleast some playback is there
                        
                        if(self?.currentState == .PAUSED || self?.currentState == .IDLE) {
                            AVPlayerIntegrationWrapper.logDebugStatement(" -- tick LATENCY: Report PLAYING --")
                            self?.timeoutOccurred()
                            self?.mmSmartStreaming.report(MMPlayerState.PLAYING);
                            self?.currentState = .PLAYING
                            if(self?.sessionInStartedState == false){
                                self?.sessionInStartedState = true;
                                self?.lastPlaybackPos = currentPlaybackPos
                                self?.lastPlaybackPosRecordTime = currentRecordTime
                            }
                        }
                    }
                    
                    if (self?.sessTerminated == true) {
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Forcing restart of session via Play .., Replay Session]")
                        self?.continueStoppedSession()
                        if( self?.loadEventTriggered == false) {
                            self?.mmSmartStreaming.reportUserInitiatedPlayback()
                            self?.loadEventTriggered = true
                        }
                    }
                }
                
                guard let lastPlaybackPos = self?.lastPlaybackPos else {
                    return;
                }
                
                guard let lastPlaybackPosRecordTime = self?.lastPlaybackPosRecordTime else {
                    return;
                }
                
                if(self?.sessionInStartedState == true) {
                    if ((player.rate  == 0  && abs(lastPlaybackPos - currentPlaybackPos) > AVPlayerIntegrationWrapper.KMinPlaybackPosDriftForPausedStateMSec) || abs(lastPlaybackPos - currentPlaybackPos) > AVPlayerIntegrationWrapper.KMinPlaybackPosDriftForPlayingStateMSec) { // We are not in buffering
                        //Check for possibility of occurrence of seek
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick process possibility of occurrence of seek \(currentRecordTime) - \(lastPlaybackPosRecordTime) = \(currentRecordTime - lastPlaybackPosRecordTime)")
                        
                        if (AVPlayerIntegrationWrapper.isPossibleSeek(currpos: currentPlaybackPos, lastPos: lastPlaybackPos, currentRecordTime: currentRecordTime, lastRecInstant: lastPlaybackPosRecordTime)) {
                            if (self?.sessTerminated == true){
                                AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Seek: Forcing restart of session via Seek ...")
                                self?.continueStoppedSession()
                            }
                            self?.reportPlayerSeekCompleted(seekEndPos: Int(currentPlaybackPos))
                        }
                    } else {
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick process possibility of occurrence of seek playhead drift \(abs(lastPlaybackPos - currentPlaybackPos))")
                    }
                }
                self?.lastPlaybackPos = currentPlaybackPos
                self?.lastPlaybackPosRecordTime = currentRecordTime
            }
        }
    }
}
